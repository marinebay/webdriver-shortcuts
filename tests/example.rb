require File.dirname(__FILE__) + '/../wd_shortcuts.rb'

# Instantiate custom wd handler
gdriver = Gscript.new
url = 'http://google.com'
driver = gdriver.get_driver
wait = gdriver.get_wait

driver.navigate.to url

element = { :id => 'lst-ib' }

gdriver.wait_for_element element 

gdriver.type element, 'gscript.com'

button = { :css => "input[value='Google Search']" }

gdriver.submit_form button

link = { :link => "gScript.com" }

gdriver.wait_for_and_click link

puts gdriver.wait_for_title

driver.close

