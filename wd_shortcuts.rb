require 'rubygems'
require 'selenium-webdriver'

class Gscript
  def initialize
    caps = Selenium::WebDriver::Remote::Capabilities.htmlunit(:browser_name => "internet explorer", :javascript_enabled => true, :version => "IE8")
    @wd_driver = Selenium::WebDriver.for(:remote, :url => "http://127.0.0.1:4444/wd/hub", :desired_capabilities => caps)
    #@wd_driver = Selenium::WebDriver.for(:remote, :url => "http://127.0.0.1:4444/wd/hub", :desired_capabilities => :firefox)
    #@wd_driver = Selenium::WebDriver.for(:remote, :url => "http://127.0.0.1:4444/wd/hub", :desired_capabilities => :ie)
    #@wd_driver = Selenium::WebDriver.for :firefox
    @wd_wait = Selenium::WebDriver::Wait.new :timeout => 240 
    @wd_url = 'http://gscript.com'
    @wd_attr
    @wd_ele
    @wd_str
  end

  def get_driver
    @wd_driver
  end

  def get_wait
    @wd_wait
  end

  def get_url
    @wd_url
  end
   
  def set_element locator
    get_element = @wd_driver.find_element locator
    return get_element
  end 

  def click locator
    driver = @wd_driver
    driver.find_element( locator ).click
  end
   
  def wait_for_and_click locator 
    driver = @wd_driver
    @wd_wait.until { driver.find_element locator }
    driver.find_element( locator ).click
  end

  def wait_for_element locator
    driver = @wd_driver
    @wd_wait.until { driver.find_element locator }
    element = driver.find_element locator
    return element
  end

  def wait_for_element_text locator
    driver = @wd_driver
    result = @wd_wait.until {
      result = driver.find_element(locator).text
      result if result.length > 0
    }
    return result
  end

  def get_element_text locator
    element = @wd_driver.find_element( locator ).text
    return element
  end  
  
  def submit_form button
    driver = @wd_driver
    button = driver.find_element button
    if button.attribute('type') == 'submit' 
      button.submit
    else 
      button.click
    end
  end 	  

  def wait_for_and_submit locator 
    driver = @wd_driver
    @wd_wait.until { driver.find_element locator }
    button = self.set_element locator
    if button.attribute('type') == 'submit' 
      button.submit
    else 
      button.click
    end
  end

  def type locator, string
    driver = @wd_driver
    driver.find_element( locator ).clear
    driver.find_element( locator ).send_keys string 
  end
  
  def wait_for_and_type locator, string
    driver = @wd_driver
    @wd_wait.until { driver.find_element locator }
    driver.find_element( locator ).clear
    driver.find_element( locator ).send_keys string 
  end

  def select_option locator, option_value 
    driver = @wd_driver
    @wd_wait.until { driver.find_element locator }
    element = driver.find_element locator
    options = element.find_elements :tag_name => "option"
    options.each do |el|
      if el.text == option_value 
        el.click
        break
      end
    end
  end

  def loop_through_options select_locator, button_locator, reporter_locator
    driver = @wd_driver
    
    self.wait_for_element select_locator
    select_element = driver.find_element select_locator
    options = select_element.find_elements :tag_name => "option"

    self.wait_for_element button_locator
    button_element = driver.find_element button_locator 

    options.each do |el|
      puts el.text
      el.click
      button_element.click
      self.wait_for_element reporter_locator
      el.click
    end
  end

  def get_elements_by_tag_name container, tag_name
    driver = @wd_driver
    @wd_wait.until { driver.find_element container }
    element = self.set_element container
    elements = element.find_elements :tag_name => tag_name
    return elements
  end

  def loop_run_report_through_checkbox_filters container, button, reporter
    driver = @wd_driver
    elements = self.get_elements_by_tag_name container, 'input'
    @wd_wait.until { driver.find_element button }
    button = driver.find_element button

    # Run report through filters.
    elements.each { |item| 
      puts item.attribute('id')
      if item.displayed?
        item.click
      end
      if button.attribute('type') == 'submit' 
        button.click
      else 
        button.click
      end
      @wd_wait.until { driver.find_element reporter }
    }
  end

  def click_through_reporter_column_headings reporter_container
    driver = @wd_driver
    @wd_wait.until { driver.find_element reporter_container }

    elements = self.get_elements_by_tag_name reporter_container, 'th'
    element_ids = []

    elements.each { |item|
      onclick = item.attribute('onclick')
      displayed = item.displayed?
      if onclick && displayed
        element_ids.push item.attribute('id')
      end
    }

    element_ids.each { |id|
      puts id
      @wd_wait.until { driver.find_element :id => id }
      driver.find_element( :id => id ).click
    }
  end

  def click_through_reporter_headings_use_date_range reporter_container, start_date, end_date, time_period, button
    driver = @wd_driver
    @wd_wait.until { driver.find_element button }
    button = driver.find_element button

    self.type start_date, time_period[:start]
    self.type end_date, time_period[:end]

    self.submit_form button
    @wd_wait.until { driver.find_element reporter_container }

    elements = self.get_elements_by_tag_name reporter_container, 'th'
    element_ids = []

    elements.each { |item|
      onclick = item.attribute('onclick')
      displayed = item.displayed?
      if onclick && displayed
        element_ids.push item.attribute('id')
      end
    }

    element_ids.each { |id|
      puts id
      @wd_wait.until { driver.find_element :id => id }
      driver.find_element( :id => id ).click
    }
  end

  def clear locator
    @wd_driver.find_element( locator ).clear
  end

  def get_attribute_value locator, attribute
    @wd_driver.find_element( locator ).attribute( attribute )
  end

  def wait_for_title
    driver = @wd_driver
    @wd_wait.until { driver.title }
  end
end
